---
layout: job_page
title: "Success Engineer"
---

Success Engineers provide direction and specialist knowledge in applying
GitLab’s solutions advantages that address our client’s business requirements.
Success Engineers are responsible for actively driving and managing the
technology evaluation and validation stages of the sales process, working in
conjunction with GitLab’s sales team as the key solutions and technical advisors
and product advocates for GitLab’s Enterprise Edition.

Success Engineers serve as a trusted advisor to the client, with
particular focus on addressing the technical requirements and challenges that
limit the customer’s ability to achieve their business objectives in driving an
effective Indirect Sales model. 

## Responsibilities:

- Development, tailoring and delivery of client-specific product demonstrations
- Provide GitLab solution Subject Matter Expertise to the client throughout the sales process
- Effectively team with GitLab sales team to assess and address technical requirements and challenges throughout the sales process
- Effectively lead and drive the response to functional and technical elements of RFIs/RFPs
- Serve as liaison between the client and GitLab Service Engineers and Product Marketing to effectively convey and prioritize customer requirements
- Become a Subject Matter Expert in competitive solution assessment and response
- Provide technical and solutions leadership in the Field to our clients, partners and sales teams
- Be the Subject Matter Expert and facilitate training workshops externally and internally.
- Provide guidance on documentation

## Requirements:

- B.Sc. in Computer Science or equivalent experience
- Above average knowledge of Unix and Unix based Operating Systems
- Vast experience with Ruby on Rails Applications and git
- Vast experience in Software Development and Developer Tool Pipeline
- Great people skills and presentation skills
- You share our [values](/handbook/#values), and work in accordance with those values
